package br.com.bentosoft.padrao.webservice;

import java.io.Serializable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

@Component
@Path("/")
@CrossOrigin(origins = "*", maxAge = 3600, allowCredentials = "*", allowedHeaders = "*")
public class LoginWebService implements Serializable {

    private static final long serialVersionUID = 5675358171931558250L;

    @GET
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@QueryParam("usuario") String usuario, @QueryParam("senha") String senha) {
        try {
            if (usuario == null || senha == null) {
                return Response.status(Status.INTERNAL_SERVER_ERROR).entity("Usuário e senha são orbigatórios.").build();
            }
            return Response.ok().entity(String.format("'Usuario': '%s' | 'Senha': '%s'", usuario, senha)).build();
        } catch (Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
