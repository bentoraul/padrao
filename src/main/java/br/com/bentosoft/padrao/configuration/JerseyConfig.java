package br.com.bentosoft.padrao.configuration;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@Configuration
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(RequestContextFilter.class);
        packages("br.com.bentosoft.padrao.webservice");
        register(LoggingFeature.class);
        // register(AuthenticationFilter.class);
        register(JacksonJsonProvider.class);
    }

}
